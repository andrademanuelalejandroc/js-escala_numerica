![Escala numerica](http://www.madarme.co/portada-web.png)

# Titulo de proyecto: js-escala_numerica

# Tabla de contenido

1. [Ejemplo](#ejemplo)
2. [Características](#Caracteristicas)
3. [Tecnologías](#Tecnologicas)
4. [IDE](#IDE)
5. [Demo](#Demo)
6. [Contribución](#Contribucion)
7. [Autor](#Autor)

## ejemplo
A continuacion ejemplo del funcionamiento de una Escala numerica.
Se muestra tabla con datos referentes Escala del Numero :1


| Dato 1 | Dato 2 | Resultado |
|--------|--------|-----------|
| 1      | 2      | 2         |
| 2      | 2      | 4         |


Institución Educativa
Escala numérica

Estudiante ing sistemas -UFPS<br>
Manuel Alejandro Coronel Andrade - 1151806<br>
Programación web -  1155606-B